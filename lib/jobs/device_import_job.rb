class DeviceImportJob < ActiveJob::Base
  queue_as :default

  def perform(customer, file, opts = {})
    import_service = DeviceImportService.new(customer)
    import_service.run(file, opts)
    FileUtils.rm(@file_path) if import_service.log_import[:errors].none?
  end
end
