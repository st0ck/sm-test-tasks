class ServiceLog
  def initialize
    @raw_messages = []
  end

  def add_message(message, type)
    @raw_messages << { type: type, message: message }
  end

  # Human-readable messages format
  def full_messages
    raw_messages.map { |el| "[#{ el[:type].upcase }] #{ el[:message] }" }
  end

  def messages(type)
    raw_messages.
      select { |el| el[:type] == type }.
      map { |el| el[:message] }
  end

  def to_hash
    @raw_messages.each_with_object({}) do |el, hsh|
      hsh[el[:type]] ||= []
      hsh[el[:type]] << el[:message]
    end
  end

  def merge!(other_log)
    @raw_messages += other_log.raw_messages
  end

  protected

  attr_reader :raw_messages
end
