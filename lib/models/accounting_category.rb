class AccountingCategory < ActiveRecord::Base
  belongs_to :accounting_type

  before_save :normalize_data

  private

  # Additionaly we should normalize existing names in db
  def normalize_data
    name.strip! if name
  end
end
