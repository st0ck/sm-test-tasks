class Device < ActiveRecord::Base
  belongs_to :customer
  has_and_belongs_to_many :accounting_categories

  scope :active, -> { where("status <> 'cancelled'") }

  def track!(_params, &block)
    block.call
  end
end
