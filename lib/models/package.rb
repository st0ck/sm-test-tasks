class Package < ActiveRecord::Base
  has_many :package_actions
  validates_associated :package_actions
end
