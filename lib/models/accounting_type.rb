class AccountingType < ActiveRecord::Base
  has_many :accounting_categories
  belongs_to :customer
end
