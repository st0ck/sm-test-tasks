# Devices API controller.
class DevicesController
  # TODO! specify in router only for POST-method
  # Imports data from provided resources
  def import
    if params[:import_file]
      create_import_job
      flash[:notice] = I18n.t('device_import.job_added')
    else
      flash[:error] = I18n.t('device_import.no_import_file')
    end
  end

  private

  # Creates job for importing data
  def create_import_job
    new_path = File.join(IMPORT_JOB_DIRECTORY, SecureRandom.hex(10))
    FileUtils.cp(params[:import_file].tempfile.path, new_path)

    DeviceImportJob.perform_later(
      customer,
      new_path,
      params
    )
  end

  # TODO! rewrite in a right way
  def customer
    @customer = Customer.find(1)
  end
end
