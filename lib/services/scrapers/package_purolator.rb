module Scrapers
  class PackagePurolator
    include Capybara::DSL
    include DataNormalization
    include ServiceLogger

    def initialize(number)
      @number = number.to_s
    end

    def fetch
      data = {}

      validate_number

      load_page
      loaded_data = Nokogiri::HTML(page.html)

      data[:package] = fetch_package_info(loaded_data)
      data[:actions] = fetch_actions_info(loaded_data)

      data
    end

    def url
      @_url = "http://www.purolator.com/purolator/ship-track/tracking-details.page?pin=#{@number}"
    end

    private

    def load_page
      page.reset_session!
      return if page.visit(url)['status'] == 'success'

      add_error(I18n.t('parser.failed_response'), true)
    end

    def fetch_package_info(page)
      {
        number: @number,
        status: page.css('#status').text,
        delivery_service: page.css('#product').text,
        received_by: page.css('#receivedBy').text,
        address_from: address_from(page),
        address_to: address_to(page),
        unit: weight_unit(page.css('#weight').text),
        weight: weight_num(page.css('#weight').text),
        shipped_at: parse_date_time(page.css('#createdDate').text),
        delivered_at: parse_date_time(page.css('#deliveryDate').text)
      }
    end

    def address_from(page)
      "#{ page.css('#origAddress1').text }, #{ page.css('#origAddress2').text }"
    end

    def address_to(page)
      "#{ page.css('#destAddress1').text }, #{ page.css('#destAddress2').text }"
    end

    def fetch_actions_info(page)
      page.css('#history.dataTable tbody tr').map do |el|
        date_time = action_date_time(el)
        {
          id_key: date_time.to_i.to_s,
          location: el.css('td.location').text,
          description: el.css('td.description').text,
          date: date_time
        }
      end
    end

    def action_date_time(el)
      parse_date_time(
        "#{ el.css('td.shipDate').text } #{ el.css('td.time').text }"
      )
    end

    def validate_number
      return if @number && @number.to_s =~ /\d{12}/

      add_error(I18n.t('parser.wrong_number_format', number: @number), true)
    end
  end
end
