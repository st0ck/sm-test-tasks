module DeviceImportValidations
  extend ActiveSupport::Concern
  include DataNormalization

  included do
    class ServiceException < Exception; end

    def validate_file_content
      validate_file_header(lookups.keys)

      numbers = []
      CSV.parse(file_content, csv_parcing_options).each_with_index do |row, idx|
        number = normalize_number(row['number'])
        valid = validate_number(number, idx, false)

        validate_accounting_stuff(row)
        numbers << number if valid
      end

      validate_numbers(numbers)
      stop_if_it_has_errors
    end

    def validate_numbers(numbers)
      validate_device_duplication(numbers, false)
      validate_existance_active_devices(numbers, false)
      validate_row_uniqueness(numbers, false)
    end

    def validate_accounting_stuff(row)
      hsh = normalize_data_values(row.to_hash)
      hsh.each do |k, v|
        next unless lookups.key?(k)
        next unless k =~ /accounting_categories/

        val = lookups[k][v.to_s.strip]
        add_new_type_error(k, v, false) unless val
      end
    end

    def validate_device_record(record)
      return if record.valid?

      record.errors.full_messages.each do |message|
        add_error(message, record.number, false)
      end
    end

    def validate_row_uniqueness(numbers, exceptional = true)
      return true if numbers.count == numbers.uniq.count

      add_error(
        I18n.t('device_import.entry_duplication'),
        'data_uniqueness',
        exceptional
      )
      false
    end

    def validate_number(number, idx, exceptional = true)
      return true unless '' == number.to_s.strip

      add_error(
        I18n.t('device_import.entry_without_number', line_num: idx),
        'number',
        exceptional
      )

      false
    end

    def validate_file_header(valid_keys)
      row = CSV.parse(file_content, headers: true, encoding: 'UTF-8').first
      return unless row

      row.headers.each do |header|
        type = header[/accounting_categories\[([^\]]+)\]/, 1]

        next unless type
        next if valid_keys.include?(header)

        add_error(I18n.t('device_import.invalid_accounting_type', type: type))
      end
    end

    def validate_file_path
      return if File.file?(@file_path)
      add_error(I18n.t('device_import.no_import_file'))
    end

    def validate_device_duplication(numbers, exceptional = true)
      doubles = active_devices(numbers).where.not(customer_id: @customer.id)
      return unless doubles.any?

      message = I18n.t('device_import.number_duplication')

      doubles.find_each do |device|
        add_error(message, device.number, false)
      end

      raise(ServiceException, message) if exceptional
    end

    def validate_existance_active_devices(numbers, exceptional = true)
      invalid_devices = active_devices(numbers).where(customer_id: @customer.id)
      return unless invalid_devices.any?

      message = I18n.t('device_import.already_active_number')

      invalid_devices.find_each do |device|
        add_error(message, device.number, false)
      end

      raise(ServiceException, message) if exceptional
    end

    def active_devices(numbers)
      Device.unscoped.active.where(number: numbers)
    end

    # ==========================================================================
    # ADD ERROR HELPERS
    # ==========================================================================

    def add_error(error, key = 'general', raise_error = true)
      @log_import[:errors][key] ||= []
      @log_import[:errors][key] << error

      raise(ServiceException, error) if raise_error
    end

    def stop_if_it_has_errors
      invalid = @log_import[:errors].any?
      add_error(I18n.t('device_import.invalid_importing_data')) if invalid
    end

    def add_new_type_error(header, value, exceptional = true)
      category_name = header.gsub(/accounting_categories\[(.*?)\]/, '\1')
      msg_opts = { category_name: category_name, code: value.to_s.strip }
      message = I18n.t('device_import.new_type', msg_opts)
      add_error(message, "wrong_data", exceptional)
    end
  end
end
