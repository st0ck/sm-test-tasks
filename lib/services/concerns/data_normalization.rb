module DataNormalization
  extend ActiveSupport::Concern

  STATUS_MAP = {}

  included do
    def normalize_number(raw_number)
      raw_number.to_s.gsub(/\D+/, '')
    end

    def reduce_equal_sign(val)
      return val unless val.is_a?(String)
      val[/^="(.*?)"/, 1] || val
    end

    def convert_boolean(val)
      %w(t f).include?(val) ? (val == 't') : val
    end

    def weight_unit(data)
      data[/(\D+)/, 1].strip.chomp('.')
    end

    def weight_num(data)
      data[/(\d+)/, 1]
    end

    def parse_date_time(string)
      return if string.blank?
      Time.parse(string).utc
    end

    def normalize_string(string)
      string.to_s.strip
    end

    def normalize_status(status)
      STATUS_MAP[status.downcase.to_sym] || status.downcase
    end
  end
end
