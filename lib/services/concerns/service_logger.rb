class ServiceLoggerException < Exception; end

# Provides ability to log information during the execution of the service
module ServiceLogger
  extend ActiveSupport::Concern

  included do
    def service_log
      @_log ||= ServiceLog.new
    end

    # Add notice message shortcut
    def add_notice(message, raise_exception = false)
      add_message(message, :notices, raise_exception)
    end

    # Add error message shortcut
    def add_error(message, raise_exception = false)
      add_message(message, :errors, raise_exception)
    end

    # Add warning message shortcut
    def add_warning(message, raise_exception = false)
      add_message(message, :warnings, raise_exception)
    end

    # Add message to logger
    def add_message(message, type, raise_exception = false)
      service_log.add_message(message, type)

      raise(ServiceLoggerException, message) if raise_exception
    end

    # Shortcut for checking if errors exists
    def errors?
      service_log.messages(:errors).present?
    end

    # Add message to logger
    def merge_log(log)
      service_log.merge!(log)
    end

    # Catches all runtime exceptions and saves it into log
    def fails_catch(&block)
      block.call
    rescue ServiceLoggerException => e
      add_error(e.message)
    rescue StandardError => e
      add_error(e.message)
      add_message(e.backtrace, :details)
    ensure
      after_hook
    end

    # Autatically calls after block call if fails_catch used
    # May be overriden if needed
    def after_hook
    end
  end
end
