require 'csv'

# В этом варианте чуть больше оверхеда по тактам процессора, но более понятная
# структура кода. Тут каждая комнада делает свой выбор чем пожертвовать.
# Этот вариант хорош в случае, если мощностей для обработки данных достаточно и
# есть вероятность, что в этом коде придется периодически что-то править.
# Кроме того, здесь мы получаем полный отчет об ошибках в файле.

# Imports devices from csv file
class DeviceImportServiceV2
  include DeviceImportValidations
  include DataNormalization

  attr_reader :log_import

  # TODO! Specify by real fields
  # format: hash
  # keys:
  #   :method - Customer relation method for handling depended model records
  #   :device_id_field - Device foreign key referencing to related model
  #   :export_key - (optional) method in customer related model uniquely
  #                 discribing record
  # Ex: [
  #   { method: :business_accounts, device_id_field: :business_account_id },
  #   { method: :carrier_bases, device_id_field: :carrier_base_id },
  #   { method: :contacts, device_id_field: :contact_id },
  #   { method: :device_makes, device_id_field: :device_make_id },
  #   { method: :device_models, device_id_field: :device_model_id }
  # ]
  # Relation map for getting access to device related models
  CUSTOMER_RELATION_MAP = []

  # Inits importer
  def initialize(customer, current_user)
    @customer = customer
    @current_user = current_user
  end

  # Starts import process
  # @param file_path [Hash] path to saved file
  # @param params [Hash] import settings
  def run(file_path, params = {})
    @log_import = { errors: {} }
    @file_path = file_path

    start_import_process(params)
  rescue ServiceException => e
    add_error(e.message, 'general', false)
  rescue StandardError => e
    add_error(e.message, 'general', false)
    add_error(e.backtrace, 'details', false)
  ensure
    save_process_result
  end

  private

  def start_import_process(params)
    validate_file_content

    data = prepare_data

    update_data(data, params)
  end

  def csv_parcing_options
    { headers: true, encoding: 'UTF-8' }
  end

  def file_content
    return @_data if @_data
    validate_file_path
    @_data ||= File.new(@file_path).read.encode(invalid: :replace, replace: '')
  end

  def lookups
    @_lookups ||= fetch_lookups
  end

  # ============================================================================
  # DATABASE MODIFICATIONS
  # ============================================================================

  def update_data(data, params)
    clear_existing_data = params[:clear_existing_data]

    track_params = {
      created_by: @current_user,
      source: I18n.t('device_import.bulk_import')
    }

    Device.transaction do
      data.each do |_number, line|
        line.track!(track_params) { line.save }
      end

      if clear_existing_data
        @customer.devices.where.not(number: data.keys).destroy_all
      end
    end
  end

  def save_process_result
    succeeded = @log_import[:errors].none?
    file_path = succeeded ? @file_path : ''

    JobResult.create(
      succeeded: succeeded,
      details: @log_import,
      source: file_path,
      job_name: 'device_importing',
      initiator: @current_user.id
    )
  end

  # ============================================================================
  # DATA PREPARING
  # ============================================================================

  def parse_csv
    data = {}

    CSV.parse(file_content, csv_parcing_options).each_with_index do |row|
      hsh = normalize_data_values(row.to_hash)

      hsh.merge!('customer_id' => @customer.id)
      hsh = convert_accounting_categories(hsh)

      data[hsh['number']] = hsh.dup
    end

    data
  end

  def convert_accounting_categories(record)
    categories = []

    record.each do |k, v|
      next unless lookups.key?(k)

      if k =~ /accounting_categories/
        categories << lookups[k][v.to_s.strip]
      else
        record[k] = lookups[k][v]
      end
    end

    record['accounting_category_ids'] = categories if categories.any?

    record
  end

  def prepare_data_to_update_db(data)
    records = {}

    @customer.devices.where(number: data.keys).find_each do |device|
      records[device.number] = device
    end

    data.each do |number, attributes|
      records[number] ||= Device.new

      attributes_for_update = attributes.slice(*Device.column_names)
      records[number].assign_attributes(attributes_for_update)

      validate_device_record(records[number])
    end

    stop_if_it_has_errors

    records
  end

  def prepare_data
    data = parse_csv
    prepare_data_to_update_db(data)
  end

  # ============================================================================
  # BUILDING LOOKUPS
  # ============================================================================

  # Gather customer related lookups
  def customer_related_lookups
    CUSTOMER_RELATION_MAP.each_with_object({}) do |opts, lookups|
      accessor = opts[:export_key] || 'name'
      data = @customer.send(opts[:method]).pluck(accessor, :id)
      lookups[opts[:device_id_field]] = Hash[data]
    end
  end

  # Gather lookup data
  def fetch_lookups
    lookups = {}

    lookups['device_make_id'] = Hash[DeviceMake.pluck(:name, :id)]
    lookups['device_model_id'] = Hash[DeviceModel.pluck(:name, :id)]

    lookups.merge(customer_related_lookups)

    @customer.accounting_types.each do |at|
      at_data = Hash[at.accounting_categories.pluck(:name, :id)]
      lookups["accounting_categories[#{ at.name }]"] = Hash[at_data]
    end

    lookups
  end

  # ============================================================================
  # DATA NORMALIZATION
  # ============================================================================

  def normalize_data_values(record)
    record['number'] = normalize_number(record['number'])

    result = record.each_with_object([]) do |(k, v), res|
      next unless k
      if v
        val = reduce_equal_sign(v)
        val = convert_boolean(val)
      end
      res << [k, val]
    end
    Hash[result]
  end
end
