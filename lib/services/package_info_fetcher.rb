class PackageInfoFetcher
  include DataNormalization
  include ServiceLogger

  SOURCE_MAPPING = {
    purolator: Scrapers::PackagePurolator
  }

  PACKAGE_STRINGS = %i(
    number status delivery_service received_by address_from address_to unit
  )

  ACTION_STRINGS = %i(location description id_key)

  def initialize(current_user)
    @current_user = current_user
  end

  def fetch_info(number, source_key = 'purolator')
    fails_catch do
      @fether = SOURCE_MAPPING[source_key.to_sym].new(number)
      fetch
    end
  end

  private

  def fetch
    data = @fether.fetch

    merge_log(@fether.service_log)
    return if errors?

    save(data)
  end

  def after_hook
    JobResult.create(
      succeeded: !errors?,
      details: service_log.to_hash,
      source: @fether.url,
      job_name: 'package_scraping',
      initiator: @current_user.id
    )
  end

  def save(data)
    data[:package] = normalize_package_data(data[:package].symbolize_keys)

    data[:actions] = data[:actions].map do |action|
      normalize_action_data(action.symbolize_keys)
    end

    package = assign_data(data)
    try_to_save(package)
  end

  def assign_data(data)
    package = Package.find_by_number(data[:package][:number]) || Package.new
    package.assign_attributes(data[:package])

    filter_new_actions(package, data[:actions]).each do |action|
      package.package_actions.build(action)
    end

    package
  end

  def filter_new_actions(package, actions)
    keys = package.package_actions.pluck(:id_key)
    actions.select { |action| keys.exclude?(action[:id_key]) }
  end

  def try_to_save(package)
    if package.valid?
      package.save
    else
      package.errors.full_messages.each do |msg|
        add_error(msg)
      end
    end
  end

  def normalize_package_data(package)
    PACKAGE_STRINGS.each do |key|
      package[key] = normalize_string(package[key])
    end

    package[:status] = normalize_status(package[:status])
    package
  end

  def normalize_action_data(action)
    ACTION_STRINGS.each do |key|
      action[key] = normalize_string(action[key])
    end
    action
  end
end
