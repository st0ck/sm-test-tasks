require 'csv'

# Imports devices from csv file
class DeviceImportService
  class ServiceException < Exception; end

  attr_reader :log_import

  # TODO! Specify by real fields
  # format: hash
  # keys:
  #   :method - Customer relation method for handling depended model records
  #   :device_id_field - Device foreign key referencing to related model
  #   :export_key - (optional) method in customer related model uniquely
  #                 discribing record
  # Ex: [
  #   { method: :business_accounts, device_id_field: :business_account_id },
  #   { method: :carrier_bases, device_id_field: :carrier_base_id },
  #   { method: :contacts, device_id_field: :contact_id },
  #   { method: :device_makes, device_id_field: :device_make_id },
  #   { method: :device_models, device_id_field: :device_model_id }
  # ]
  # Relation map for getting access to device related models
  CUSTOMER_RELATION_MAP = []

  # Inits importer
  def initialize(customer, current_user)
    @customer = customer
    @current_user = current_user
  end

  # Starts import process
  # @param file_path [Hash] path to saved file
  # @param params [Hash] import settings
  def run(file_path, params = {})
    @log_import = { errors: {}, warnings: [] }
    @file_path = file_path

    start_import_process(params)
  rescue ServiceException => e
    add_error(e.message, 'general', false)
  rescue StandardError => e
    add_error(e.message, 'general', false)
    add_error(e.backtrace, 'details', false)
  ensure
    save_process_result
  end

  private

  def start_import_process(params)
    run_basic_validations
    lookups = fetch_lookups
    contents = File.new(@file_path).read.encode(invalid: :replace, replace: '')

    validate_csv_file_header(contents, lookups)

    data = parse_csv(contents, lookups)

    validate_device_duplication(data.keys)
    validate_existance_active_devices(data.keys)

    updated_lines = prepare_data_to_update_db(data)
    update_db_data(updated_lines, params[:clear_existing_data])
  end

  # ============================================================================
  # DATABASE MODIFICATIONS
  # ============================================================================

  def update_db_data(data, clear_existing_data)
    track_params = {
      created_by: @current_user,
      source: I18n.t('device_import.bulk_import')
    }

    Device.transaction do
      data.each do |_number, line|
        line.track!(track_params) { line.save }
      end

      if clear_existing_data
        @customer.devices.where.not(number: data.keys).destroy_all
      end
    end
  end

  def save_process_result
    succeeded = @log_import[:errors].none?
    file_path = succeeded ? @file_path : ''

    JobResult.create(
      succeeded: succeeded,
      details: @log_import,
      source: file_path,
      job_name: 'device_importing',
      initiator: @current_user.id
    )
  end

  # ============================================================================
  # DATA PREPARING
  # ============================================================================

  def parse_csv(contents, lookups)
    data = {}

    pars_opts = { headers: true, encoding: 'UTF-8' }

    CSV.parse(contents, pars_opts).each_with_index do |row, idx|
      accounting_categories = []
      hsh = convert_device_record(row, idx)

      next unless row_unique?(hsh, data)

      hsh.dup.each do |k, v|
        next unless lookups.key?(k)

        if k =~ /accounting_categories/
          val = lookups[k][v.to_s.strip]

          if val
            accounting_categories << val
          else
            add_new_type_error(k, v, hsh['number'])
          end

          hsh.delete(k)
        else
          hsh[k] = lookups[k][v]
        end
      end

      unless accounting_categories.empty?
        hsh['accounting_category_ids'] = accounting_categories
      end

      data[hsh['number']] = hsh.dup
    end

    data
  end

  def prepare_data_to_update_db(data)
    updated_lines = {}

    @customer.devices.where(number: data.keys).find_each do |device|
      updated_lines[device.number] = device
    end

    data.each do |number, attributes|
      updated_lines[number] ||= Device.new

      attributes_for_update = attributes.slice(*Device.column_names)
      updated_lines[number].assign_attributes(attributes_for_update)

      next unless updated_lines[number].invalid?

      updated_lines[number].errors.full_messages.each do |message|
        add_error(message, number)
      end
    end

    updated_lines
  end

  # ============================================================================
  # DATA NORMALIZATION
  # ============================================================================

  def convert_device_record(row, idx)
    hsh = row.to_hash.merge('customer_id' => @customer.id)

    validate_number(hsh['number'], idx)

    hsh['number'].gsub!(/\D+/, '')

    result = hsh.each_with_object([]) do |(k, v), res|
      next unless k
      if v
        val = reduce_equal_sign(v)
        val = convert_boolean(val)
      end
      res << [k, val]
    end
    Hash[result]
  end

  def reduce_equal_sign(val)
    return val unless val.is_a?(String)
    val[/^="(.*?)"/, 1] || val
  end

  def convert_boolean(val)
    %w(t f).include?(val) ? (val == 't') : val
  end

  # ============================================================================
  # BUILDING LOOKUPS
  # ============================================================================

  def customer_related_lookups
    CUSTOMER_RELATION_MAP.each_with_object({}) do |opts, lookups|
      accessor = opts[:export_key] || 'name'
      data = @customer.send(opts[:method]).pluck(accessor, :id)
      lookups[opts[:device_id_field]] = Hash[data]
    end
  end

  # Gather actial lookup data
  def fetch_lookups
    lookups = {}

    lookups['device_make_id'] = Hash[DeviceMake.pluck(:name, :id)]
    lookups['device_model_id'] = Hash[DeviceModel.pluck(:name, :id)]

    lookups.merge(customer_related_lookups)

    @customer.accounting_types.each do |at|
      at_data = Hash[at.accounting_categories.pluck(:name, :id)]
      lookups["accounting_categories[#{ at.name }]"] = Hash[at_data]
    end

    lookups
  end

  # ============================================================================
  # DATA VALIDATION
  # ============================================================================

  def validate_number(number, idx)
    return true unless '' == number.to_s.strip

    add_error(I18n.t('device_import.entry_without_number', line_num: idx))
  end

  def row_unique?(row, data)
    return true unless data[row['number']]

    add_error(
      I18n.t('device_import.entry_duplication'),
      'general',
      false
    )
    false
  end

  def validate_csv_file_header(file_content, lookups)
    row = CSV.parse(file_content, headers: true, encoding: 'UTF-8').first
    return unless row

    row.headers.each do |header|
      next unless header =~ /accounting_categories\[([^\]]+)\]/
      next if lookups.key?(header)

      add_error(I18n.t('device_import.invalid_accounting_type', type: $1))
    end
  end

  def validate_customer_devices
    return unless @customer.devices.count.zero?
    add_warning(I18n.t('device_import.customer_without_device'))
  end

  def validate_customer_business_accounts
    return unless @customer.business_accounts.count.zero?
    add_warning(I18n.t('device_import.customer_without_account'))
  end

  def validate_file_path
    return if File.file?(@file_path)
    add_error(I18n.t('device_import.no_import_file'))
  end

  def run_basic_validations
    validate_file_path
    validate_customer_devices
    validate_customer_business_accounts
  end

  def validate_device_duplication(numbers)
    duplications = active_devices(numbers).where.not(customer_id: @customer.id)
    return unless duplications.any?

    message = I18n.t('device_import.number_duplication')

    duplications.find_each do |device|
      add_error(message, device.number, false)
    end

    raise ServiceException.new(message)
  end

  def validate_existance_active_devices(numbers)
    invalid_devices = active_devices(numbers).where(customer_id: @customer.id)
    return unless invalid_devices.any?

    message = I18n.t('device_import.already_active_number')

    invalid_devices.find_each do |device|
      add_error(message, device.number, false)
    end

    raise ServiceException.new(message)
  end

  def active_devices(numbers)
    Device.unscoped.active.where(number: numbers)
  end

  # ============================================================================
  # SERVICE HELPER METHODS
  # ============================================================================

  def add_error(error, key = 'general', raise_error = true)
    @log_import[:errors][key] ||= []
    @log_import[:errors][key] << error

    raise ServiceException.new(error) if raise_error
  end

  def add_warning(error)
    @log_import[:warnings] << error
  end

  def add_new_type_error(header, value, number)
    category_name = header.gsub(/accounting_categories\[(.*?)\]/, '\1')
    msg_opts = { category_name: category_name, code: value.to_s.strip }
    message = I18n.t('device_import.new_type', msg_opts)
    add_error(message, number)
  end
end
