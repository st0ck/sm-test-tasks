require 'sqlite3'
require 'active_record'

# Connect to an in-memory sqlite3 database
ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: 'sm_test.db'
)

# Define a minimal database schema
ActiveRecord::Schema.define do
  tables = %i(accounting_categories_devices customers)

  tables.each { |name| create_table(name, force: true) }

  create_table :business_accounts, force: true do |f|
    f.references :customer, index: true
  end

  create_table :job_results, force: true do |f|
    f.boolean :succeeded, null: false, default: false
    f.text :details # f.json for postgresql
    f.string :source
    f.string :job_name
    f.integer :initiator
  end

  create_table :devices, force: true do |f|
    f.references :customer, index: true
    f.string :status
    f.string :number
  end

  create_table :accounting_categories_devices, force: true do |f|
    f.references :device, index: true
    f.references :accounting_category, index: true
  end

  create_table :device_makes, force: true do |f|
    f.string :name
  end

  create_table :device_models, force: true do |f|
    f.string :name
  end

  create_table :accounting_types, force: true do |f|
    f.references :customer, index: true
    f.string :name
  end

  create_table :accounting_categories, force: true do |f|
    f.references :accounting_type, index: true
    f.string :name
  end

  create_table :packages, force: true do |f|
    f.string :number
    f.string :status
    f.string :delivery_service
    f.string :received_by
    f.string :address_from
    f.string :address_to
    f.string :unit
    f.float :weight
    f.datetime :shipped_at
    f.datetime :delivered_at
  end

  create_table :package_actions, force: true do |f|
    f.references :package
    f.string :id_key
    f.string :location
    f.string :description
    f.datetime :date
  end
end
