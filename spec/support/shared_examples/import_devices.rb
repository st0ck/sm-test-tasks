RSpec.shared_examples 'basic device import' do
  it { expect { subject }.to change(JobResult, :count).by(1) }

  it 'changes count of devices' do
    if job_result_status
      expect { subject }.to change(Device, :count).by(count_new_records)
    end
  end

  it 'import has succeeded status' do
    subject
    expect(JobResult.last.succeeded).to eq(job_result_status)
  end
end

RSpec.shared_examples 'import with clear old data' do
  it { expect(Device.count).to be > 0 }

  it 'deletes all existed and active customer related devices' do
    ids = Device.where(customer: customer).pluck(:id)
    subject
    expect(Device.where(id: ids).count).to eq(0)
  end

  it { expect(Device.count).to eq(count_new_records) }
end
