RSpec.describe ServiceLog do
  let(:log) { ServiceLog.new }
  let(:count_messages) { rand(5) + 2 }
  let(:messages) { count_messages.times.map { SecureRandom.hex } }
  let(:types) { count_messages.times.map { SecureRandom.hex } }

  subject { log }

  before do
    messages.zip(types).each { |msg, type| log.add_message(msg, type) }
  end

  it { expect(subject.full_messages).to be_an Array }
  it { expect(subject.messages(:any_type)).to be_an Array }

  describe 'instance methods' do
    describe '#add_message' do
      subject { log.add_message('Some message', :type) }

      it { expect { subject }.to change { log.full_messages.count } }
    end

    describe '#full_messages' do
      it 'should include all messages' do
        log.full_messages.zip(messages).each do |res, msg|
          expect(res).to include(msg)
        end
      end

      it 'should include all types' do
        log.full_messages.zip(types).each do |res, type|
          expect(res.downcase).to include(type)
        end
      end
    end

    describe '#messages' do
      let(:types) do
        count_messages.times.map { "key#{ rand(count_messages / 2) }" }
      end

      let(:type_count) do
        types.each_with_object(Hash.new(0)) { |e, h| h[e] += 1 }
      end

      it 'returns messages only passed type' do
        types.each do |type|
          expect(log.messages(type).count).to eq(type_count[type])
        end
      end
    end

    describe '#merge!' do
      let(:other_log) { ServiceLog.new }
      let(:uniq_type) { :uniq_type }

      subject { log.messages(uniq_type).count }

      before { other_log.add_message('some msg', uniq_type) }

      it { is_expected.to be == 0 }

      it 'should add messages from other log' do
        log.merge!(other_log)
        is_expected.to be > 0
      end
    end

    describe '#to_hash' do
      it { expect(log.to_hash).to be_a Hash }
    end
  end
end
