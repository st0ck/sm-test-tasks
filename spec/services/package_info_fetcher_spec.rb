RSpec.describe Scrapers::PackagePurolator do
  let(:number) { '330599320893' }
  let(:file_path) { "spec/fixtures/scraping/packages/#{ number }.html" }
  let(:html) { File.open(file_path) }
  let(:user) { double }
  let(:service) { PackageInfoFetcher.new(user) }

  subject { service.fetch_info(number) }

  before do
    allow(user).to receive(:id).and_return(1)

    allow_any_instance_of(Capybara::Session).
      to receive(:visit).and_return('status' => 'success')
    allow_any_instance_of(Capybara::Session).
      to receive(:html).and_return(html)
  end

  describe '#fetch_info' do
    it { expect { subject }.to change(JobResult, :count).by(1) }
    it { expect { subject }.to change(Package, :count).by(1) }
    it { expect { subject }.to change(PackageAction, :count).by(5) }

    it 'should create succeeded job result' do
      subject
      expect(JobResult.last.succeeded).to be_truthy
    end

    context 'when package already exist' do
      let(:package) { create(:package, number: number) }
      let(:package_action) do
        create(:package_action, id_key: '1448898540', package: package)
      end

      it { expect { subject }.to change { package.reload.inspect } }

      it 'should keep all old package actions' do
        expect { subject }.not_to change { package_action.reload.inspect }
      end

      it 'should add new package actions' do
        expect { subject }.
          to change(PackageAction, :count).by(5 - PackageAction.count)
      end
    end

    context 'when got unnormolized data' do
      let(:status) { '   DeLiVEreD  ' }
      let(:num) { '330599320893  ' }
      let(:location) { '123, LA  ' }
      let(:action) { { id_key: '123123123', location: location } }

      let(:data) do
        {
          package: { status: status, number: num },
          actions: [action]
        }
      end

      before do
        allow_any_instance_of(Scrapers::PackagePurolator).
          to receive(:fetch).and_return(data)

        subject
      end

      it { expect(Package.last.status).to eq(status.downcase.strip) }
      it { expect(Package.last.number).to eq(num.strip) }
      it { expect(PackageAction.last.location).to eq(location.strip) }
    end

    context 'when passed incorrect number' do
      let(:num) { nil }
      let(:msg) { I18n.t('parser.wrong_number_format', number: num) }
      subject { service.fetch_info(num) }

      it { expect { subject }.not_to raise_error }
      it { expect { subject }.to change(JobResult, :count).by(1) }

      it 'job resutl contains failed status' do
        subject
        expect(JobResult.last.succeeded).to be_falsey
      end

      it 'job resutl contains error' do
        subject
        expect(JobResult.last.details).to include(msg)
      end
    end
  end
end
