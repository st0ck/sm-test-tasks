# К этому тесту хорошо бы еще хук сделать, который будет на CI автоматом
# страничку с определенным трек-номером (330599320893) сохранять для оперативной
# валидации изменения верстки

RSpec.describe Scrapers::PackagePurolator do
  let(:number) { '330599320893' }
  let(:file_path) { "spec/fixtures/scraping/packages/#{ number }.html" }
  let(:html) { File.open(file_path) }
  let(:service) { Scrapers::PackagePurolator.new(number) }

  subject { service.fetch }

  before do
    allow_any_instance_of(Capybara::Session).
      to receive(:visit).and_return('status' => 'success')
    allow_any_instance_of(Capybara::Session).
      to receive(:html).and_return(html)
  end

  describe 'scrapping' do
    it { is_expected.to be_a(Hash) }

    describe 'pakage result' do
      let(:excepted_keys) { %w(id) }
      let(:attributes) { Package.attribute_names - excepted_keys }
      let(:result_keys) { subject.stringify_keys.keys }

      subject { service.fetch[:package] }

      it { expect(result_keys).to match_array(attributes) }

      it 'values shouldn`t be blank' do
        no_empty = subject.values.none?(&:blank?)
        expect(no_empty).to be_truthy
      end

      it { expect(subject[:number]).to eq(number) }
      it { expect(subject[:status]).to eq('Delivered') }
      it { expect(subject[:delivery_service]).to eq('Purolator Express') }
      it { expect(subject[:received_by]).to eq('Samira') }
      it { expect(subject[:weight]).to eq('1') }
      it { expect(subject[:unit]).to eq('lb') }
      it { expect(subject[:shipped_at]).to eq(Time.parse('Nov 30, 2015').utc) }

      it 'delivered_at should has correct value' do
        expect(subject[:delivered_at]).
          to eq(Time.parse('Dec 1, 2015 8:31 a.m.').utc)
      end

      it 'address_from should has correct value' do
        expect(subject[:address_from]).
          to eq('3910 GATEWAY BLVD NW, Edmonton, AB, CA')
      end

      it 'address_to should has correct value' do
        expect(subject[:address_to]).
          to eq('3731 52 AVE E, Edmonton International Airport, AB, CA')
      end
    end

    describe 'pakage actions result' do
      let(:excepted_keys) { %w(id package_id) }
      let(:attributes) { PackageAction.attribute_names - excepted_keys }
      let(:result_keys) { subject.stringify_keys.keys }

      subject { service.fetch[:actions].first }

      it { expect(result_keys).to match_array(attributes) }

      it 'values shouldn`t be blank' do
        no_empty = subject.values.none?(&:blank?)
        expect(no_empty).to be_truthy
      end

      it { expect(subject[:id_key]).to eq(subject[:date].to_i.to_s) }
      it { expect(subject[:location]).to eq('Edmonton (south/sud), AB') }

      it 'date should has correct value' do
        expect(subject[:date]).to eq(Time.parse('Dec 1, 2015 8:31 a.m.').utc)
      end

      it 'description should has correct value' do
        expect(subject[:description]).
          to eq('Shipment delivered to SAMIRA at: RECEPTION')
      end
    end

    describe 'parsing errors' do
      let(:result_errors) do
        normalize = JobResult.last.details.
          gsub(/:([a-zA-z]+)/, '"\\1"').gsub('=>', ': ')
        JSON.parse(normalize)['errors']
      end

      context 'when passing Numeric value' do
        let(:number) { 330_599_320_893 }

        it 'converts number into string' do
          service.fetch
          expect(service.service_log.messages(:errors)).to be_empty
        end
      end

      context 'when passing wrong number' do
        let(:html) { '' }
        let(:msg) { I18n.t('parser.wrong_number_format', number: number) }

        context 'when passing abracadabra' do
          let(:number) { 'abracadabra1' }

          it { expect { subject }.to raise_error(ServiceLoggerException, msg) }
        end

        context 'when passing empty string' do
          let(:number) { '' }

          it { expect { subject }.to raise_error(ServiceLoggerException, msg) }
        end

        context 'when passing nil value' do
          let(:number) { nil }

          it { expect { subject }.to raise_error(ServiceLoggerException, msg) }
        end
      end

      context 'when returned responce error' do
        let(:msg) { I18n.t('parser.failed_response') }

        before do
          allow_any_instance_of(Capybara::Session).
            to receive(:visit).and_return('status' => 'failed')
        end

        it { expect { subject }.to raise_error(ServiceLoggerException, msg) }
      end
    end
  end
end
