RSpec.describe DeviceImportService do
  let(:params) { {} }
  let(:service) { DeviceImportService.new }

  subject { service }

  describe 'CUSTOMER_RELATION_MAP' do
    let(:customer) { create(:customer) }

    let(:device_foreign_keys) do
      suitable_columns = Device.column_names - blacklist
      suitable_columns.find_all { |c| c =~ /^(\w+)_id/ }
    end

    let(:blacklist) do
      %w(customer_id model_id device_model_mapping_id carrier_rate_plan_id)
    end

    subject { DeviceImportService::CUSTOMER_RELATION_MAP }

    DeviceImportService::CUSTOMER_RELATION_MAP.each do |rec|
      it { expect(customer).to respond_to(rec[:method]) }

      next unless rec[:export_key]

      it 'should contain valid export_keys for device relation model' do
        export_key = rec[:export_key].to_s
        related_model = Customer.reflections[rec[:method]].active_record
        expect(related_model.method_defined?(export_key)).to be_truthy
      end
    end

    it 'should contain all device foreign keys except blacklist' do
      mapped_keys = subject.map { |rec| rec[:device_id_field].to_s }
      expect(mapped_keys).to match_array(device_foreign_keys)
    end
  end

  describe 'instance methods' do
    let(:customer) { create(:customer) }
    let(:user) { double }
    let(:file_path) { 'spec/fixtures/devices/import_2_devices.csv' }
    let(:params) { Hash.new }
    let(:existed_numbers) { %w(4038283663 4038269268) }

    let(:accounting_data) do
      {
        'Reports To' => [
          '10010 Corporate Development',
          '10083 INT - International',
          '26837 Carson Wainwright'
        ],
        'Cost Center' => [
          '10083.8350',
          '6837.7037.18',
          '26837.7037.18',
          '10010.8350'
        ]
      }
    end

    subject { DeviceImportService.new(customer, user).run(file_path, params) }

    before do
      allow(user).to receive(:id).and_return(1)
      accounting_data.each do |type_name, category_names|
        at = create(:accounting_type, customer: customer, name: type_name)
        category_names.each do |name|
          create(:accounting_category, accounting_type: at, name: name)
        end
      end

      existed_numbers.each do |num|
        create(:device, number: num, customer: customer, status: 'active')
      end
    end

    describe '#run' do
      let(:job_result_status) { true }
      let(:count_new_records) { 2 }

      it_behaves_like 'basic device import'

      context 'when importing devices existed' do
        let(:file_path) { 'spec/fixtures/devices/import_4_devices.csv' }

        context 'and active' do
          let(:job_result_status) { false }

          it_behaves_like 'basic device import'
        end

        context 'and cancelled' do
          before { Device.all.update_all(status: 'cancelled') }

          it_behaves_like 'basic device import'
        end
      end

      context 'when passing clear_existing_data param' do
        let(:params) { { clear_existing_data: true } }

        it_behaves_like 'import with clear old data'
      end

      describe 'handling warnings and errors' do
        let(:before_parse) {}

        let(:result) do
          normalize = JobResult.last.details.
            gsub(/:([a-zA-z]+)/, '"\\1"').gsub('=>', ': ')
          JSON.parse(normalize)
        end

        before do
          before_parse
          subject
        end

        context 'when customer have no devices' do
          let(:before_parse) { customer.devices.destroy_all }

          it 'contain <no devices> warning' do
            expect(result['warnings']).
              to include(I18n.t('device_import.customer_without_device'))
          end
        end

        context 'when customer have no business accounts' do
          let(:before_parse) { customer.business_accounts.destroy_all }

          it 'contain <no business_account> warning' do
            expect(result['warnings']).
              to include(I18n.t('device_import.customer_without_account'))
          end
        end

        context 'when import file is not exist' do
          let(:file_path) { '' }

          it 'contain <file is not exist> error' do
            expect(result['errors']['general']).
              to include(I18n.t('device_import.no_import_file'))
          end
        end

        context 'when file contains wrong accounting type' do
          let(:file_path) { 'spec/fixtures/devices/import_alien_acc_type.csv' }

          it 'contain <wrong accounting type> error' do
            msg = I18n.t('device_import.invalid_accounting_type', type: 'Alien')
            expect(result['errors']['general']).to include(msg)
          end
        end

        context 'when file doesnt contains some numbers' do
          let(:file_path) { 'spec/fixtures/devices/import_no_number.csv' }

          it 'contain <no number> error' do
            msg = I18n.t('device_import.entry_without_number', line_num: 0)
            expect(result['errors']['general']).to include(msg)
          end
        end

        context 'when file contains duplicate numbers' do
          let(:file_path) do
            'spec/fixtures/devices/import_entry_duplication.csv'
          end

          it 'contain <duplicate numbers> error' do
            expect(result['errors']['general']).
              to include(I18n.t('device_import.entry_duplication'))
          end
        end

        context 'when file contains new accounting category' do
          let(:file_path) { 'spec/fixtures/devices/import_new_category.csv' }

          it 'contain <new accounting category> error' do
            opts = { category_name: 'Cost Center', code: '10019.8350' }
            msg = I18n.t('device_import.new_type', opts)
            expect(result['errors']['general']).to include(msg)
          end
        end

        context 'when file contains duplicate numbers' do
          let(:file_path) { 'spec/fixtures/devices/import_duplication.csv' }
          let(:num) { '123456' }

          let(:before_parse) do
            customer = create(:customer)
            create(:device, customer: customer, status: 'active', number: num)
          end

          it 'contain <duplicate numbers> error' do
            expect(result['errors']['general']).
              to include(I18n.t('device_import.number_duplication'))
          end
        end

        context 'when file contains already active number' do
          let(:file_path) do
            'spec/fixtures/devices/import_already_active_number.csv'
          end

          let(:before_parse) do
            create(:device, number: num, customer: customer, status: 'active')
          end

          let(:num) { '123123123' }

          it 'contain <already active number> error' do
            expect(result['errors']['general']).
              to include(I18n.t('device_import.already_active_number'))
          end
        end
      end
    end
  end
end
